// Reference:http://faq.cprogramming.com/cgi-bin/smartfaq.cgi?id=1043284392&answer=1046053421
            //for converting string to all lowercase.


#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
using std::string;
#include <iostream>
using std::cin;
using std::cout;
#include <set>
using std::set;
using std::multiset;
#include<vector>
using std::vector;
using namespace std;
#include <string.h>
#include <algorithm>





static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of sort.  Supported options:\n\n"
"   -r,--reverse        Sort descending.\n"
"   -f,--ignore-case    Ignore case.\n"
"   -u,--unique         Don't output duplicate lines.\n"
"   --help              Show this message and exit.\n";

struct igncaseComp {
	bool operator()(const string& s1, const string& s2) {
		return (strcasecmp(s1.c_str(),s2.c_str()) < 0);
	}
};
/* NOTE: set<string,igncaseComp> S; would declare a set S which
 * does its sorting in a case-insensitive way! */

 void sortU() //use set to exclude extra duplicated lines
 {
     set<string>inputList;
    string input;
    while(getline(cin,input))
    {
        if(input=="%%")
            {
                break;
            }
            else
            {
                inputList.insert(input);
            }
    }
    for(set<string>::iterator i=inputList.begin();i!=inputList.end();i++)
    {
        cout<<*i<<endl;
    }
 }
void sortR() //reverse alphabetically
{
     //sort -r begin
    multiset<string, greater<string> >set1; //use greater to reverse order
    // from z to a
    string input;
    while(getline(cin,input))
    {
        if(input=="%%")
            {
                break;
            }
            else
            {
                set1.insert(input);
            }
    }
    for(set<string>::iterator i=set1.begin();i!=set1.end();i++)
    {
        cout<<*i<<endl;
    }

    //sort -r end
}

void sortF() //ignore the uppercase and sort
{
    //sort -f begin
    multiset<string>inputList;
     //reverse order
    // from z to a
    string input;
    while(getline(cin,input))
    {
        transform(input.begin(), input.end(), input.begin(), (int (*)(int))tolower); //online reference
        if(input=="%%")
            {
                break;
            }
            else
            {
                inputList.insert(input);
            }
    }
    for(set<string>::iterator i=inputList.begin();i!=inputList.end();i++)
    {
        cout<<*i<<endl;
    }
}
int main(int argc, char *argv[]) {
	// define long options
	static int descending=0, ignorecase=0, unique=0;
	static struct option long_opts[] = {
		{"reverse",       no_argument,   0, 'r'},
		{"ignore-case",   no_argument,   0, 'f'},
		{"unique",        no_argument,   0, 'u'},
		{"help",          no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "rfuh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'r':
				descending = 1;
				sortR();
				break;
			case 'f':
				ignorecase = 1;
				sortF();
				break;
			case 'u':
				unique = 1;
				sortU();
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	/* TODO: write me... */





	return 0;
}
