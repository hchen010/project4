#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
using std::string;
#include <iostream>

using namespace std;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of uniq.  Supported options:\n\n"
"   -c,--count         prefix lines by their counts.\n"
"   -d,--repeated      only print duplicate lines.\n"
"   -u,--unique        only print lines that are unique.\n"
"   --help             show this message and exit.\n";

void ShowCount()
{
    string currentInput;
	string previousInput;

	int counter=1;
	int stacker=0;
	getline(cin,previousInput);
	while(getline(cin,currentInput))
    {
        if(currentInput!=previousInput)
        {
            cout<<counter<<" "<<previousInput<<endl;
            previousInput=currentInput;
            getline(cin,currentInput);
        }
        while(currentInput==previousInput)
        {
            stacker=1;
            counter++;
            previousInput=currentInput;
            getline(cin,currentInput);
            if(previousInput!=currentInput)
            {
                cout<<counter<<" "<<previousInput<<endl;
                counter=1;

            }
        }
        if(stacker!=1) //this will only excute if above isn't true;
        {
            cout<<counter<<" "<<previousInput<<endl;
        }
        previousInput=currentInput;

    }
}
void Duplicated()   //uniq -d work as intended
{
    string currentInput;
	string previousInput;
	string nextInput;
	int counter=1;
	getline(cin,previousInput);
	while(getline(cin,currentInput))
    {
        if(currentInput!=previousInput)
        {

            previousInput=currentInput;
            getline(cin,currentInput);
        }
        while(currentInput==previousInput)
        {
            counter++;
            previousInput=currentInput;
            getline(cin,currentInput);
            if(previousInput!=currentInput && counter>1)
            {
                cout<<counter<<" "<<previousInput<<endl;
                counter=1;

            }
        }
        previousInput=currentInput;
    }

}

void PrintUniq() //-u works as intended
{
    string currentInput;
	string previousInput;
	string nextInput;
	int counter=1;
	getline(cin,previousInput);
	while(getline(cin,currentInput))
    {

        if(previousInput!=currentInput && counter==1)
        {
            cout<<"printed "<<previousInput<<endl;
            previousInput=currentInput;
        }
        else
        {
            counter++;
            previousInput=currentInput;
            getline(cin,currentInput);
            if(previousInput!=currentInput)
            {
                counter=1;
                previousInput=currentInput;
            }

        }




    }
}

int main(int argc, char *argv[]) {
	// define long options
	static int showcount=0, dupsonly=0, uniqonly=0;
	static struct option long_opts[] = {
		{"count",         no_argument, 0, 'c'},
		{"repeated",      no_argument, 0, 'd'},
		{"unique",        no_argument, 0, 'u'},
		{"help",          no_argument, 0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "cduh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				showcount = 1;
				ShowCount();
				break;
			case 'd':
				dupsonly = 1;
				Duplicated();
				break;
			case 'u':
				uniqonly = 1;
				PrintUniq();
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	/* TODO: write me... */
	// uniq -c done -------------------------------------------uniq -c







        /*idea:
        #1 take first input, if next input equal to first,appear counter+1, take next input if
        still equal +1 until they are not equal and print the final counter + the word

        else print 1 and first input then compare the future input with current one. repeat
            steps on 1

        */



















	return 0;

}


