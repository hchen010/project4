/*
 * CSc103 Project 3: unix utilities
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours:
 */
#include <iostream>
#include <string>
#include <set>/
#include <vector>
#include <getopt.h> // to parse long arguments.
#include <cstdio>

using namespace std;
// printf


static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of wc.  Supported options:\n\n"
"   -c,--bytes            print byte count.\n"
"   -l,--lines            print line count.\n"
"   -w,--words            print word count.\n"
"   -L,--max-line-length  print length of longest line.\n"
"   -u,--uwords           print unique word count.\n"
"   --help          show this message and exit.\n";

void wcCommand(char myChar)
{
    vector <vector <string> > myText;
    vector<string>myLine;
    string input;
    string temp;
    int byteCount=0;
    int lineCount=0;
    int wordCount=0;
    int nospace=0;
    int longestline=0;

    int charSize=sizeof('a');
    cout<<"Enter your inputs, when finished type in %% to end"<<endl;
    while(getline(cin,input))
    {

        if(input=="%%") //to exit loop when finish lines
        {
            break;
        }
        else
        {
           lineCount++;
           input+=" "; //add space to each endline or system will combined them
           myLine.push_back(input);
           myText.push_back(myLine);
        }


    }
    for(int i=0;i<myText.size();i++)
    {

            temp=myText[i][i];//loop through string in the program.

            if(temp.length()-1>longestline)
            {
                longestline=temp.length()-1; //get the longestline.
            }

            for(int k=0;k<temp.length();k++)
            {

                if(temp[k]==' ')
                {
                    wordCount++;
                    nospace++;
                }
                byteCount++;

                //cout<<temp[k];

            }
            if(nospace==0)
            {
                wordCount--;
            }

    }

    switch(myChar) //print based on given char parameter
    {
        case 'c':
            cout<<byteCount-lineCount<<endl;
            break;
        case 'w':
            cout<<wordCount<<endl;
            break;
        case 'l':
            cout<<lineCount<<endl;
            break;
        case 'L':
            cout<<longestline<<endl;
            break;


    }

}

int main(int argc, char *argv[])
{
	// define long options
	static int charonly=0, linesonly=0, wordsonly=0, uwordsonly=0, longonly=0;
	static struct option long_opts[] = {
		{"bytes",           no_argument,   0, 'c'},
		{"lines",           no_argument,   0, 'l'},
		{"words",           no_argument,   0, 'w'},
		{"uwords",          no_argument,   0, 'u'},
		{"max-line-length", no_argument,   0, 'L'},
		{"help",            no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "clwuLh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				wcCommand('c');
				break;
			case 'l':
				wcCommand('l');
				break;
			case 'w':
				wcCommand('w');
				break;
			case 'u':
				uwordsonly = 1;
				break;
			case 'L':
				wcCommand('L');
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	/* TODO: write me... */
    //unique words, use "sets" since it take only unique value****
    //word byte count- reading character and take it's easy. use "int something" to store the bite value
    /*
    vector <vector <string> > myText;
    vector<string>myLine;
    string input;
    string temp;
    int byteCount=0;
    int lineCount=0;
    int wordCount=0;
    int nospace=0;
    int longestline=0;

    int charSize=sizeof('a');
    cout<<"Enter your inputs, when finished type in %% to end"<<endl;
    while(getline(cin,input))
    {

        if(input=="%%") //to exit loop when finish lines
        {
            break;
        }
        else
        {
           lineCount++;
           input+=" "; //add space to each endline or system will combined them
           myLine.push_back(input);
           myText.push_back(myLine);
        }


    }

    for(int i=0;i<myText.size();i++)
    {

            temp=myText[i][i];//loop through string in the program.

            if(temp.length()-1>longestline)
            {
                longestline=temp.length()-1;
            }

            for(int k=0;k<temp.length();k++)
            {

                if(temp[k]==' ')
                {
                    wordCount++;
                    nospace++;
                }
                byteCount++;

                cout<<temp[k];

            }
            if(nospace==0)
            {
                wordCount--;
            }
            cout<<endl;
    }


    cout<<lineCount<<" "<<wordCount<<" "<<byteCount-lineCount<<" "<<longestline<<endl;

    //word line count- whenever "\n" appear increase the line by one
    //word count- wheneven a white space is encounter " " then word increase by one.


    */

	return 0;

}
