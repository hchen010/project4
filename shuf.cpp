 #include <cstdio>   // printf
#include <cstdlib>  // rand
#include <time.h>   // time
#include <getopt.h> // to parse long arguments.
#include <stdlib.h>
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <iostream>
using std::cin;
using std::cout;
#include <algorithm>
using std::swap;
using std::min;
using namespace std;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of shuf.  Supported options:\n\n"
"   -e,--echo              treat each argument as an input line.\n"
"   -i,--input-range=LO-HI treat each number in [LO..HI] as an input line.\n"
"   -n,--head-count=N      output at most N lines.\n"
"   --help                 show this message and exit.\n";



void ShufE() //swap words and print it;
{   cout<<"Enter your inputs and type %% to end"<<endl;
    vector<string>shufE;
    string words;
    while(cin>>words)
    {
        if(words=="%%")
        {
            break;
        }
        else{
            shufE.push_back(words);
        }
    }
    srand(time(0));
    for(int i=0;i<shufE.size();i++)
    {
        swap(shufE[rand()%shufE.size()],shufE[rand()%shufE.size()]);
        //swap random index in shufE
    }


    for(int i=0;i<shufE.size();i++)
    {
        cout<<shufE[i]<<"\n";
    }

}

void ShufI() //print random number within given range from stdin;
{

    cout<<"enter lower boundary"<<endl;
    int lowest;
    cin>>lowest;
    cout<<"enter higher boundary"<<endl;
    int highest;
    cin>>highest;

    for(int i=0;i<highest-lowest;i++)
    {
        cout<<rand()%(highest-lowest+1)+lowest<<" ";
        //give range from lowest boundary to highest boundary;
    }
}

void ShufN() //print random input line from user;
{
    vector<string>shufN;
    string myLines;
    cout<<"Enter your inputs and type %% to end"<<endl;
    while(getline(cin,myLines))
    {
        if(myLines=="%%")
        {
            break;
        }
        else
        {
            shufN.push_back(myLines);
        }
    }
    cout<<shufN[rand()%shufN.size()];
}
int main(int argc, char *argv[]) {
	// define long options


	static struct option long_opts[] = {
		{"echo",        no_argument,       0, 'e'},
		{"input-range", required_argument, 0, 'i'},
		{"head-count",  required_argument, 0, 'n'},
		{"help",        no_argument,       0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "einh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'e':
				//echo = 1;
				void ShufE(); //shuf e
				break;

			case 'i':
				void ShufI(); //shuf -i
				break;
			case 'n':
				//count = atol(optarg);
				void ShufN(); //shuf -n
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}
	/* NOTE: the system's shuf does not read stdin *and* use -i or -e.
	 * Even -i and -e are mutally exclusive... */

	/* TODO: write me... */
    // shuffle among numeric input shuf
    //-------------------------------------------
    /*
    while(cin>>words)
    {
        if(words=="%%")
        {
            break;
        }
        else{
            shufE.push_back(words);
        }
    }
    srand(time(0));
    for(int i=0;i<shufE.size();i++)
    {
        swap(shufE[rand()%shufE.size()],shufE[rand()%shufE.size()]);
        //swap random index in shufE
    }


    for(int i=0;i<shufE.size();i++)
    {
        cout<<shufE[i]<<"\n";
    }

    int lowest;
    cin>>lowest;
    int highest;
    cin>>highest;
    for(int i=0;i<highest-lowest;i++)
    {
        cout<<rand()%(highest-lowest+1)+lowest<<" ";
        //give range from lowest boundary to highest boundary;
    }

    //-n shuffle random line
    vector<string>shufN;
    string myLines;
    while(getline(cin,myLines))
    {
        if(myLines=="%%")
        {
            break;
        }
        else
        {
            shufN.push_back(myLines);
        }
    }
    cout<<shufN[rand()%shufN.size()]; //print one of the random line from string vector

    */
    // Ci can't get the command line to work properly here. So I use the following to test codes
    char input;
    cout<<"You can enter 'e','i' or 'n'"<<endl;
    cin>>input;
    switch(input)
    {
        case 'e':
            ShufE();
            break;
        case 'i':
            ShufI();
            break;
        case 'n':
            ShufN();
            break;
    }


	return 0;
}
